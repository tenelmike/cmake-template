#include "mainwindow.h"
#include <QApplication>

#include <boost/program_options/options_description.hpp>
#include <boost/program_options/option.hpp>

#include <iostream>

using namespace std;

namespace po = boost::program_options;

int main(int argc, char *argv[])
{
    po::options_description desc("Allowed options");
    desc.add_options()("help", "Produces this help message");

    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
